require "test_helper"

class UserCardTest < Minitest::Test
  include ActionView::Component::TestHelpers

  def setup
    @user = User.create \
      first_name: "Kasper",
      last_name: "Meyer",
      occupation: "Seniorudvikler",
      residence: "Odense",
      birthday: Date.parse("1988/12/29")
  end

  def test_full_name
    output = render_inline(UserCard, user: @user)

    assert_equal %(<h1 class="title">Kasper Meyer</h1>), output.css("h1").to_html
  end
end
