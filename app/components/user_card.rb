class UserCard < ActionView::Component::Base
  def initialize(user:)
    @user = user
  end

  def avatar_url
    ImageService.avatar_url_for(user)
  end

  def full_name
    "#{user.first_name} #{user.last_name}"
  end

  def summary
    "#{age} • #{user.residence} • #{user.occupation}"
  end

  private

    def age
      ((Time.zone.now - user.birthday.to_time) / 1.year.seconds).floor
    end

    def user
      @user
    end
end
